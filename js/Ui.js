class UI {
  constructor() {
    this.modal = document.querySelector(".modal-produto");
    this.effect = document.querySelector("body > div.bg.ds-none");
    this.input = document.querySelector("#cart-value");
    this.span = document.querySelector(".cart-qnt");
    this.btn = document.querySelector(".btn-comprar");
    this.close = document.querySelector("#btn-close");
    this.value = parseInt(this.input.value);
    this.init();
  }

  init() {
    try {
      this.btn.addEventListener("click", e => {
        e.preventDefault();
        if (this.modal.classList.contains("ds-none")) {
          this.effect.classList.remove("ds-none");
          this.modal.classList.remove("ds-none");
          this.value += 1;
          this.span.innerHTML = this.value;
        }
      });

      this.close.addEventListener("click", e => {
        e.preventDefault();
        if (this.modal) {
          this.effect.classList.add("ds-none");
          this.modal.classList.add("ds-none");
        }
      });
    } catch (e) {
      console.error(e);
    }
  }
}
